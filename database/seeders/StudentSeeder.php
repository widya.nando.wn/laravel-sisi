<?php

namespace Database\Seeders;

use App\Models\Classroom;
use App\Models\Student;
use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Classroom::create([
            'code'=>'A-01',
            'name'=>'Algoritma Dasar'
        ]);

        Classroom::create([
            'code'=>'B-02',
            'name'=>'Komunikasi Profesional'
        ]);

        Classroom::create([
            'code'=>'C-03',
            'name'=>'Geomatika'
        ]);

        Student::factory(10)->create();
    }
}
