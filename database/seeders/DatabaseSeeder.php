<?php

namespace Database\Seeders;

use App\Models\BlogCategory;
use App\Models\BlogPost;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();
        
        // User::create([
        //     'name'=>'Einzuji',
        //     'email'=>'ein@gmail.com',
        //     'password'=>Hash::make('12345')
        // ]);


        BlogCategory::create([
            'name'=>'Personal',
            'slug'=>'personal'
        ]);

        BlogCategory::create([
            'name'=>'Programming',
            'slug'=>'programming'
        ]);

        BlogPost::factory(15)->create();

        $this->call([
            StudentSeeder::class,
        ]);
    }
}
