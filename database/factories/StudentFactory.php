<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nrp' => $this->faker->nik(),
            'name' => $this->faker->name(),
            'major' => $this->faker->jobTitle(),
            'user_id' => $this->faker->randomDigitNotNull(),
        ];
    }
}
