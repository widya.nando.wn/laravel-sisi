<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\StudentCollection;
use App\Http\Resources\StudentResource;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $student = Student::paginate(5);
        return (StudentResource::collection($student)->additional(['message'=>'success'])
        ->response()->setStatusCode(200));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'username' => 'required|unique:users|min:3|max:255',
            'email' => 'required|unique:users|email:dns',
            'password' => 'required|min:6|max:255|confirmed'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);       
        }

        $validator1 = Validator::make($request->all(),[
            'nrp'=>'required|string|min:16|max:16|unique:students,nrp',
            'name'=>'required',
            'major'=>'required',
            'classroom_id'=>'required|array|min:1',
            'classroom_id.*'=>'required|numeric|distinct|exists:classrooms,id',
        ]);

        if($validator1->fails()){
            return response()->json($validator1->errors(), 400);       
        }

        $validated = $validator->validated();
        $validated1 = $validator1->validated();

        $validated['password'] = Hash::make($validated['password']);
        $user = User::create($validated);
        $user->isStudent = true;
        $user->save();
        
        $student = new Student($validated1);

        $student->user()->associate($user);
        $student->save();

        $student->classrooms()->attach($validated1['classroom_id']);

        return (new StudentResource($student))
        ->response()->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return (new StudentResource($student))
        ->response()->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $validator = Validator::make($request->all(),[
            'nrp'=>"required|string|min:16|max:16|unique:students,nrp,$student->id",
            'name'=>'required',
            'major'=>'required',
            'classroom_id'=>'required|array|min:1',
            'classroom_id.*'=>"required|numeric|unique:classroom_student,classroom_id,classroom_id,student_id,student_id,$student->id|distinct|exists:classrooms,id",
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);       
        }

        $validated = $validator->validated();
        $student->update($validated);

        $student->classrooms()->attach($validated['classroom_id']);
        return (new StudentResource($student))
        ->response()->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->user->delete();
        $student->delete();
        return response()->noContent();
    }
}
