<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'username' => 'required|unique:users|min:3|max:255',
            'email' => 'required|unique:users|email:dns',
            'password' => 'required|min:6|max:255|confirmed'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);       
        }

        $validated = $validator->validated();
        $validated['password'] = Hash::make($validated['password']);
        $user = User::create($validated);

        return response()
            ->json(['data' => $user,'message'=>'Register success, please login for token access', ]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email'=>'required|email:dns',
            'password'=>'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);       
        }

        if (!Auth::attempt($request->only('email', 'password')))
        {
            return response()
                ->json(['message' => 'Invalid Credentials'], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()
            ->json(['data' => $user->id,'message' => 'Login Success','access_token' => $token, 'token_type' => 'Bearer', ]);
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();

        return response() 
                ->json(['message' => 'Logout success'], 200);
    }
}
