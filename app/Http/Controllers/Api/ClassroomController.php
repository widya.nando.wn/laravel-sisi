<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ClassroomResource;
use App\Models\Classroom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClassroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classroom = Classroom::all();
        return (ClassroomResource::collection($classroom))->additional(['message'=>'success'])
        ->response()->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'code'=>'required|string|min:4|max:4|unique:classrooms,code',
            'name'=>'required|unique:classrooms,name'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);       
        }

        $validated = $validator->validated();
        $classroom = Classroom::create($validated);
        return (new ClassroomResource($classroom))
        ->response()->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Classroom $classroom)
    {
        return (new ClassroomResource($classroom))
        ->response()->setStatusCode(200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  Classroom $classroom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Classroom $classroom)
    {
        $validator = Validator::make($request->all(),[
            'code'=>"required|string|min:4|max:4|unique:classrooms,code,$classroom->id",
            'name'=>"required|unique:classrooms,name,$classroom->id"
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);       
        }

        $validated = $validator->validated();
        $classroom->update($validated);
        return (new ClassroomResource($classroom))
        ->response()->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  Classroom $classroom
     * @return \Illuminate\Http\Response
     */
    public function destroy(Classroom $classroom)
    {
        $classroom->delete();
        return response()->noContent();
    }
}
