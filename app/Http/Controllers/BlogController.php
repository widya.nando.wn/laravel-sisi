<?php

namespace App\Http\Controllers;

use App\Models\BlogCategory;
use App\Models\BlogPost;
use App\Models\User;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(){
        $title = 'Blog';
        if(request('c')){
            $category = BlogCategory::firstwhere('slug', request('c'));
            $title = $category->name;
        }

        if(request('a')){
            $author = User::firstwhere('username', request('a'));
            $title = "by " . $author->username;
        }

        $context = [
            "title" => $title,
            "blog_posts" => BlogPost::latest()->myfilter(request(['q', 'c', 'a']))->paginate(4)->withQueryString()
        ];
        return view('blog.index', $context);
    }

    public function detail(BlogPost $blog_post){
        $context = [
            "title" => "Single post",
            "blog_post" => $blog_post
        ];
        return view('blog.blogdetail', $context);
    }
}
