<?php

namespace App\Http\Controllers;

use App\Models\BlogCategory;
use App\Models\BlogPost;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DashboardBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog_posts = BlogPost::where('user_id', auth()->user()->id)->get();
        $context = [
            "title" => "Blog Dashboard",
            "blog_posts" => $blog_posts,
        ];
        return view('dashboard.blog.index', $context);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $context = [
            "title" => "Create Blog",
            "categories" => BlogCategory::all()
        ];
        return view('dashboard.blog.create', $context);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title'=>'required|max:255',
            'slug'=>'required|unique:blog_posts',
            'category_id'=>'required',
            'body'=>'required'
        ]);

        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['excerpt'] = Str::limit(strip_tags($request->body), 100);

        BlogPost::create($validatedData);

        return redirect('/dashboard/blog')->with('success', 'New post successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function show(BlogPost $blog_post)
    {
        // if($blog_post->user->id != auth()->user()->id){
        //     abort(403);
        // }
        $this->authorize('owner-blog', $blog_post);

        $context = [
            "title" => "Single Post",
            "blog_post" => $blog_post,
        ];
        return view('dashboard.blog.show', $context);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function edit(BlogPost $blog_post)
    {
        $this->authorize('owner-blog', $blog_post);

        $context = [
            "title" => "Edit Blog",
            "blog_post" => $blog_post,
            "categories" => BlogCategory::all()
        ];
        return view('dashboard.blog.edit', $context);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlogPost $blog_post)
    {
        $this->authorize('owner-blog', $blog_post);

        $validatedData = $request->validate([
            'title'=>'required|max:255',
            'slug'=>"required|unique:blog_posts,slug,$blog_post->id",
            'category_id'=>'required',
            'body'=>'required'
        ]);

        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['excerpt'] = Str::limit(strip_tags($request->body), 100);

        $blog_post->update($validatedData);

        return redirect('/dashboard/blog')->with('success', 'Post successfully edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BlogPost  $blogPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlogPost $blog_post)
    {
        $this->authorize('owner-blog', $blog_post);
        
        $blog_post->delete();

        return redirect('/dashboard/blog')->with('success', 'Post successfully deleted');
    }
}
