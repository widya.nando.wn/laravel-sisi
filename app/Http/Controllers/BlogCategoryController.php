<?php

namespace App\Http\Controllers;

use App\Models\BlogCategory;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    public function index(){
        $context = [
            'title'=>'Categories',
            'categories'=>BlogCategory::all()
        ];
        return view('blog.category', $context);
    }

    // public function list(BlogCategory $category){
    //     $context = [
    //         'title'=>$category->name,
    //         'blog_posts'=>$category->blogPosts
    //     ];
    //     return view('blog.blog', $context);
    // }
}
