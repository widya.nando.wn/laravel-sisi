<?php

namespace App\Http\Controllers;

use App\Models\Classroom;
use Illuminate\Http\Request;

class DashboardClassroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('admin');
        $classrooms = Classroom::all();
        $context = [
            "title" => "Classroom Dashboard",
            "classrooms" => $classrooms,
        ];
        return view('dashboard.classroom.index', $context);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $context = [
            "title" => "Create Classroom",
            "classrooms" => Classroom::all()
        ];
        return view('dashboard.classroom.create', $context);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'code'=>'required|string|min:4|max:4|unique:classrooms,code',
            'name'=>'required|unique:classrooms,name'
        ]);

        Classroom::create($validatedData);

        return redirect('/dashboard/classroom')->with('success', 'New classroom successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Classroom $classroom)
    {
        $context = [
            "title" => "Classroom Info",
            "classroom" => $classroom,
        ];
        return view('dashboard.classroom.show', $context);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Classroom $classroom)
    {
        $context = [
            "title" => "Edit Classroom",
            "classroom" => $classroom
        ];
        return view('dashboard.classroom.edit', $context);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Classroom $classroom)
    {
        $validatedData = $request->validate([
            'code'=>"required|string|min:4|max:4|unique:classrooms,code,$classroom->id",
            'name'=>"required|unique:classrooms,name,$classroom->id"
        ]);

        $classroom->update($validatedData);

        return redirect('/dashboard/classroom')->with('success', 'Classroom successfully edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Classroom $classroom)
    {
        $classroom->delete();

        return redirect('/dashboard/classroom')->with('success', 'Classroom successfully deleted');
    }
}
