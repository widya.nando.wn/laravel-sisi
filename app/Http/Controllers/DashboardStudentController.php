<?php

namespace App\Http\Controllers;

use App\Models\Classroom;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DashboardStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        $context = [
            "title" => "Student Dashboard",
            "students" => $students,
        ];
        return view('dashboard.student.index', $context);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $context = [
            "title" => "Create Student",
            "classrooms" => Classroom::all()
        ];
        return view('dashboard.student.create', $context);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'username' => 'required|unique:users|min:3|max:255',
            'email' => 'required|unique:users|email:dns',
            'password' => 'required|min:6|max:255|confirmed'
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);

        $validatedData1 = $request->validate([
            'nrp'=>'required|string|min:16|max:16|unique:students,nrp',
            'name'=>'required',
            'major'=>'required',
            'classroom_id'=>'required|exists:classrooms'
        ]);

        $user = User::create($validatedData);
        $user->isStudent = true;
        $user->save();
        // $student = Student::create($validatedData1);
        $student = new Student($validatedData1);

        $student->user()->associate($user);
        $student->save();

        return redirect('/dashboard/student')->with('success', 'New student successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        $context = [
            "title" => "Student Info",
            "student" => $student,
        ];
        return view('dashboard.student.show', $context);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        $context = [
            "title" => "Edit Student",
            "student" => $student,
            "classrooms" => Classroom::all()
        ];
        return view('dashboard.student.edit', $context);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $validatedData = $request->validate([
            'nrp'=>"required|string|min:16|max:16|unique:students,nrp,$student->id",
            'name'=>'required',
            'major'=>'required',
            'classroom_id'=>'required'
        ]);

        $student->update($validatedData);

        return redirect('/dashboard/student')->with('success', 'Student successfully edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();

        return redirect('/dashboard/student')->with('success', 'Student successfully deleted');
    }
}
