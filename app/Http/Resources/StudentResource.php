<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    // public static $wrap = 'data';
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'user_id'=>$this->user_id,
            'admin'=>$this->when($this->user->isAdmin, true),
            'nrp'=>$this->nrp,
            'name'=>$this->name,
            'major'=>$this->major,
            'created_at'=>$this->created_at,
            'updated_at'=>$this->updated_at,
            'classroom'=>ClassroomResource::collection($this->whenLoaded('classrooms')),
            'posts'=>BlogPostResource::collection($this->whenLoaded('user')->blogPosts),
        ];
    }

    public function with($request)
    {
        return [
            'message'=>'successs'
        ];
    }
}
