<?php

namespace App\Providers;

use App\Models\BlogPost;
use App\Models\Classroom;
use App\Models\Student;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin', function (User $user) {
            return $user->isAdmin;
        });

        Gate::define('student', function (User $user) {
            return $user->isStudent;
        });

        Gate::define('not-student', function (User $user) {
            return !$user->isStudent;
        });

        Gate::define('owner-profile', function (User $user) {
            return $user->id === $user->student->user_id;
        });

        Gate::define('owner-blog', function (User $user, BlogPost $blog_post) {
            return $user->id === $blog_post->user_id;
        });
    }
}
