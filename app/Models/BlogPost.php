<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    use HasFactory;

    // protected $fillable = ['title', 'excerpt', 'body'];
    protected $guarded = ['id'];
    protected $with = ['category', 'user'];

    public function scopeMyfilter($query, array $filters){
        // if(isset($filters['q']) ? $filters['q'] : false) {
        //     return $query->where('title', 'like', '%' . $filters['q'] . '%')
        //                 ->orWhere('body', 'like', '%' . $filters['q'] . '%');
        // }

        $query->when($filters['q'] ?? false, function($query, $search){
            return $query->where(function($query) use ($search) {
                $query->where('title', 'like', '%' . $search . '%')
                    ->orWhere('body', 'like', '%' . $search . '%');
            });
        });

        $query->when($filters['c'] ?? false, function($query, $category){
            return $query->whereHas('category', function($query) use ($category){
                $query->where('slug', $category);
            });
        });

        $query->when($filters['a'] ?? false, fn($query, $author)=> 
            $query->whereHas('user', fn($query)=>
                $query->where('username', $author)
            )
        );
    }

    public function category(){
        return $this->belongsTo(BlogCategory::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
