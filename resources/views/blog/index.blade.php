@extends('layouts.main')
@section('container')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">
        {{ $title }}
    </h1>
</div>
<a class="text-s text-info" href="/categories/">All Categories</a>

@if ($blog_posts->count())
<!-- Earnings (Monthly) Card Example -->
<div class="row mt-3">
    @foreach ($blog_posts as $post)
    <div class="col-lg-6 d-flex">
        <!-- Basic Card Example -->
        <div class="card shadow mb-4 flex-fill">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ $post->title }}</h6>
                <h6 class="mt-2 mb-0 text-xs text-info">
                    <a class="text-xs text-info" href="/blog?c={{ $post->category->slug }}">{{ $post->category->name }}</a> | By:
                    <a class="text-xs text-info" href="/blog?a={{ $post->user->username }}">{{ $post->user->username }}</a>
                </h6>
            </div>
            <div class="card-body">
                {{ $post->excerpt }}
                <p class="mb-0 mt-2"><a class="text-primary" href="/blog/{{ $post->slug }}">More details >></a></p>
            </div>
        </div>
    </div>
    @endforeach
</div>
@else
<p class="text-center text-lg">No post found.</p>
@endif
<!-- Earnings (Monthly) Card Example -->
@if(isset($blog_posts))
    <div class="d-flex justify-content-center">
    {{ $blog_posts->links() }}
    </div>
@endif

@endsection
            