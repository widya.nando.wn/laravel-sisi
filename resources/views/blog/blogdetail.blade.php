@extends('layouts.main')
@section('container')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">
        {{ $title }}
    </h1>
</div>

<!-- Earnings (Monthly) Card Example -->
<div class="row">
    <div class="col-lg-12 d-flex">
        <!-- Basic Card Example -->
        <div class="card shadow mb-4 flex-fill">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ $blog_post->title }}</h6>
                <h6 class="mt-2 mb-0 text-xs text-info">
                    <a class="text-xs text-info" href="/blog?c={{ $blog_post->category->slug }}">{{ $blog_post->category->name }}</a> | By:
                    <a class="text-xs text-info" href="/blog?a={{ $blog_post->user->username }}">{{ $blog_post->user->username }}</a>
                </h6>
            </div>
            <div class="card-body">
                {!! $blog_post->body !!}
                <p class="mb-0 mt-3"><a class="btn btn-outline-primary" href="/blog">Back</a></p>
            </div>
        </div>
    </div>
</div>
<!-- Earnings (Monthly) Card Example -->
@endsection