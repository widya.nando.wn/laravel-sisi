@extends('layouts.main')
@section('container')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">
        {{ $title }}
    </h1>
    <a href="#" class="btn btn-outline-primary">+ Add post</a>
</div>

<!-- Earnings (Monthly) Card Example -->
<div class="row">
    @foreach ($categories as $category)
    <div class="col-lg-6 d-flex">
        <!-- Basic Card Example -->
        <div class="card shadow mb-4 flex-fill">
            <div class="card-header py-3">
                <a class="m-0 font-weight-bold text-primary" href="/blog?c={{ $category->slug }}">{{ $category->name }}</a>
            </div>
        </div>
    </div>
    @endforeach
</div>
<!-- Earnings (Monthly) Card Example -->
@endsection
            