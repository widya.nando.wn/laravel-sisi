@extends('layouts.main')
@section('container')
<!-- Begin Page Content -->


    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">
            {{ $title }}
        </h1>
    </div>

    <div class="card o-hidden border-0 shadow-lg">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row justify-content-center">
                <div class="col-lg-3">
                    <div class="mt-3 mb-2 p-1">
                        <img class="rounded-circle" src="{{ asset('img/undraw_profile_1.svg') }}" style="max-width: 100%; height: 150px;" alt="...">
                    </div>
                    <div class="mt-3 mb-2 p-1">
                        <p>Email:</p>
                        <span class="border border-primary rounded p-2">{{ $profile->email }}</span>
                    </div>
                    <div class="mb-2 p-1">
                        <p>Username:</p>
                        <span class="border border-primary rounded p-2">{{ $profile->username }}</span>
                    </div>
                    <div class="mb-2 p-1">
                        <p>NRP:</p>
                        <span class="border border-primary rounded p-2">{{ $profile->student->nrp }}</span>
                    </div>
                    <div class="mb-2 p-1">
                        <p>Nama:</p>
                        <span class="border border-primary rounded p-2">{{ $profile->student->name }}</span>
                    </div>
                    <div class="mb-2 p-1">
                        <p>Jurusan:</p>
                        <span class="border border-primary rounded p-2">{{ $profile->student->major }}</span>
                    </div>
                    <div class="mb-4 p-1">
                        <p>Kelas:</p>
                        <span class="border border-primary rounded p-2">{{ $profile->student->classroom->name }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
@endsection