@extends('layouts.main')
@section('container')
<!-- Begin Page Content -->


    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">
            {{ $title }}
        </h1>
    </div>

    <div class="card o-hidden border-0 shadow-lg">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="p-5">
                        <!-- /help text & error -->
                        
                        <!-- forms -->
                        <form method="POST" action="/dashboard/student/{{ $student->nrp }}" autocomplete="off" enctype="multipart/form-data">
                            @method('PATCH')
                            @csrf
                            <div class="form-group">
                                <input type="text" name="nrp" class="form-control form-control-user @error('nrp') is-invalid @enderror"
                                    placeholder="Enter nrp..." value="{{ old('nrp', $student->nrp) }}" id="nrp" required>
                                @error('nrp')
                                <div class="ml-3 invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="text" name="name" class="form-control form-control-user @error('name') is-invalid @enderror"
                                    placeholder="Enter name..." value="{{ old('name', $student->name) }}" id="name" required>
                                @error('name')
                                <div class="ml-3 invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="text" name="major" class="form-control form-control-user @error('major') is-invalid @enderror"
                                    placeholder="Enter major..." value="{{ old('major', $student->major) }}" id="major" required>
                                @error('major')
                                <div class="ml-3 invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="classroom" class="form-label">classroom:</label>
                                <select class="custom-select @error('classroom_id') is-invalid @enderror" name="classroom_id" required>
                                    @foreach ($classrooms as $classroom)
                                        @if(old('classroom_id', $student->classroom_id)==$classroom->id)
                                            <option selected value="{{ $classroom->id }}">{{ $classroom->code }} | {{ $classroom->name }}</option>
                                        @else
                                            <option value="{{ $classroom->id }}">{{ $classroom->code }} | {{ $classroom->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('classroom_id')
                                <div class="ml-3 invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <button class="btn btn-primary btn-user" type="submit">Edit Student</button>
                        </form>
                        <!-- forms -->
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
@endsection