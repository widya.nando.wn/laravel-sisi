@extends('layouts.main')
@section('container')
<!-- Begin Page Content -->


    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">
            {{ $title }}
        </h1>
    </div>

    <div class="card o-hidden border-0 shadow-lg mb-4">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="p-5">
                        <!-- /help text & error -->
                        
                        <!-- forms -->
                        <form method="POST" action="/dashboard/student" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="username" class="form-control form-control-user @error('username') is-invalid @enderror"
                                    id="exampleInpuUsername" placeholder="Username" value="{{ old('username') }}">
                                @error('username')
                                <div class="ml-3 invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control form-control-user @error('email') is-invalid @enderror"
                                    id="exampleInputEmail" placeholder="Email Address" value="{{ old('email') }}">
                                @error('email')
                                <div class="ml-3 invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input type="password" name="password" class="form-control form-control-user @error('password') is-invalid @enderror" id="exampleInputPassword" placeholder="Password">
                                    @error('password')
                                    <div class="ml-3 invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-sm-6">
                                    <input type="password" name="password_confirmation" class="form-control form-control-user  @error('password_confirmation') is-invalid @enderror" id="exampleRepeatPassword" placeholder="Repeat Password">
                                    @error('password_confirmation')
                                    <div class="ml-3 invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        <!-- forms -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card o-hidden border-0 shadow-lg">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="p-5">
                        <!-- /help text & error -->
                        
                        <!-- forms -->
                            <div class="form-group">
                                <input type="text" name="nrp" class="form-control form-control-user @error('nrp') is-invalid @enderror"
                                    placeholder="Enter nrp..." value="{{ old('nrp') }}" id="nrp">
                                @error('nrp')
                                <div class="ml-3 invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="text" name="name" class="form-control form-control-user @error('name') is-invalid @enderror"
                                    placeholder="Enter name..." value="{{ old('name') }}" id="name">
                                @error('name')
                                <div class="ml-3 invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="text" name="major" class="form-control form-control-user @error('major') is-invalid @enderror"
                                    placeholder="Enter major..." value="{{ old('major') }}" id="major">
                                @error('major')
                                <div class="ml-3 invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="classroom" class="form-label">classroom:</label>
                                <select class="custom-select @error('classroom_id') is-invalid @enderror" name="classroom_id">
                                    @foreach ($classrooms as $classroom)
                                        @if(old('classroom_id')==$classroom->id)
                                            <option selected value="{{ $classroom->id }}">{{ $classroom->code }} | {{ $classroom->name }}</option>
                                        @else
                                            <option value="{{ $classroom->id }}">{{ $classroom->code }} | {{ $classroom->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @error('classroom_id')
                                <div class="ml-3 invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            
                            <button class="btn btn-primary btn-user" type="submit">Add Student</button>
                        </form>
                        <!-- forms -->
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
@endsection