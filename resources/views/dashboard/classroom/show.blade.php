@extends('layouts.main')
@section('container')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">
        {{ $title }}
    </h1>
</div>

<!-- Earnings (Monthly) Card Example -->
<div class="row">
    <div class="col-lg-12 d-flex">
        <!-- Basic Card Example -->
        <div class="card shadow mb-4 flex-fill">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{ $classroom->name }}</h6>
                <h6 class="mt-2 mb-0 text-xs text-info">
                    <p class="text-xs text-info mb-0">{{ $classroom->code }}</p>
                </h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NRP</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($classroom->students as $student)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $student->nrp }}</td>
                                <td>{{ $student->name }}</td>
                                <td>
                                    <a class="badge badge-info" href="/dashboard/student/{{ $student->nrp }}"><i class="fas fa-fw fa-eye"></i></a>
                                    <a class="badge badge-warning" href="/dashboard/student/{{ $student->nrp }}/edit"><i class="fas fa-fw fa-edit text-dark"></i></a>
                                    <button class="badge badge-danger border-0" data-toggle="modal" data-target="#mystudentDelete-{{ $student->id }}"><i class="fas fa-fw fa-trash"></i></button>
                                </td>
                            </tr>
                                <!-- delete Modal-->
                                <div class="modal fade" id="mystudentDelete-{{ $student->id }}" role="dialog">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deleteModalLabel">Confirmation</h5>
                                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">Are you sure want to delete {{ $student->name }}?</div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                                <form action="/dashboard/student/{{ $student->nrp }}" method="POST">
                                                    @method('delete')
                                                    @csrf
                                                    <button class="btn btn-danger" type="submit">Delete</a>
                                                </form>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <p class="mb-0 mt-3">
                    <a class="btn btn-outline-primary mr-2" href="/dashboard/classroom">Back</a>
                    <a class="btn btn-outline-warning mr-2" href="/dashboard/classroom/{{ $classroom->code }}/edit">Edit</a>
                    <button class="btn btn-outline-danger mr-2" data-toggle="modal" data-target="#myclassroomDelete">Delete</button>
                </p>
            </div>
        </div>
    </div>
</div>
<!-- Earnings (Monthly) Card Example -->

    <!-- delete Modal-->
    <div class="modal fade" id="myclassroomDelete" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Confirmation</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Are you sure want to delete {{ $classroom->name }}?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <form action="/dashboard/classroom/{{ $classroom->code }}" method="POST">
                        @method('delete')
                        @csrf
                        <button class="btn btn-danger" type="submit">Delete</a>
                    </form>
                </div>
            </div>
        </div>  
    </div>
@endsection