@extends('layouts.main')
@section('container')
<!-- Begin Page Content -->

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">
            {{ auth()->user()->student->name }}'s Blog Dashboard
        </h1>
        <a href="/dashboard/blog/create" class="btn btn-outline-primary">+ Add Post</a>
    </div>

    @if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show text-s" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($blog_posts as $blog_post)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $blog_post->title }}</td>
                            <td>{{ $blog_post->category->name }}</td>
                            <td>
                                <a class="badge badge-info" href="/dashboard/blog/{{ $blog_post->slug }}"><i class="fas fa-fw fa-eye"></i></a>
                                <a class="badge badge-warning" href="/dashboard/blog/{{ $blog_post->slug }}/edit"><i class="fas fa-fw fa-edit text-dark"></i></a>
                                <button class="badge badge-danger border-0" data-toggle="modal" data-target="#mydeleteBLogModal-{{ $blog_post->id }}"><i class="fas fa-fw fa-trash"></i></button>
                            </td>
                        </tr>
                            <!-- delete Modal-->
                            <div class="modal fade" id="mydeleteBLogModal-{{ $blog_post->id }}" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="deleteModalLabel">Confirmation</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">Are you sure want to delete {{ $blog_post->title }}?</div>
                                        <div class="modal-footer">
                                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                            <form action="/dashboard/blog/{{ $blog_post->slug }}" method="POST">
                                                @method('delete')
                                                @csrf
                                                <button class="btn btn-danger" type="submit">Delete</a>
                                            </form>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
    
@endsection
            