<?php

use App\Http\Controllers\BlogCategoryController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\DashboardBlogController;
use App\Http\Controllers\DashboardClassroomController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DashboardStudentController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RegisterController;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use Prophecy\Doubler\Generator\Node\ReturnTypeNode;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $context = [
        "title" => "Home"
    ];
    return view('home', $context);
});

Route::get('blog', [BlogController::class, 'index']);
Route::get('blog/{blog_post:slug}', [BlogController::class, 'detail']);

Route::get('categories', [BlogCategoryController::class, 'index']);
// Route::get('category/{category:slug}', [BlogCategoryController::class, 'list']);

// Route::get('author/{user:username}', function(User $user){
//     return view('blog.blog', [
//         'title' => "by $user->name",
//         'blog_posts' => $user->blogPosts,
//     ]);
// });

Route::get('login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('login', [LoginController::class, 'authenticate']);
Route::post('logout', [LoginController::class, 'logout']);

Route::resource('profile', ProfileController::class)->except(['index, create'])->middleware('auth')
        ->parameters(['profile'=>'user'])->scoped(['user'=>'username']);

Route::get('register', [RegisterController::class, 'index'])->middleware('guest');
Route::post('register', [RegisterController::class, 'store']);

Route::get('dashboard', [DashboardController::class, 'index'])->middleware('is_student');

Route::resource('dashboard/blog', DashboardBlogController::class)
        ->middleware('is_student')->parameters(['blog'=>'blog_post'])->scoped(['blog_post'=>'slug']);

Route::resource('dashboard/student', DashboardStudentController::class)
        ->middleware('is_admin')->scoped(['student'=>'nrp']);

Route::resource('dashboard/classroom', DashboardClassroomController::class)->middleware('is_admin')->scoped(['classroom'=>'code']);