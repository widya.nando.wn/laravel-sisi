<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ClassroomController;
use App\Http\Controllers\Api\StudentController;
use App\Http\Resources\StudentCollection;
use App\Http\Resources\StudentResource;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//API route for register new user
Route::post('/register', [AuthController::class, 'register']);
//API route for login user
Route::post('/login', [AuthController::class, 'login']);

//Protecting Routes
Route::group(['middleware' => ['auth:sanctum', 'is_admin']], function () {
    Route::resource('/student', StudentController::class);
    Route::resource('/classroom', ClassroomController::class);
});

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/profile', function(Request $request) {
        return $request->user();
    });
    // API route for logout user
    Route::post('/logout', [AuthController::class, 'logout']);
});
// Route::get('/student/{id}', function($id){
//     // return new StudentResource(Student::findOrFail($id));
// });

// Route::get('/students', function(){
//     // return (new StudentCollection(Student::paginate(5)))->additional(['message'=>'success']);
//     // return StudentResource::collection(Student::all());
//     // return new StudentCollection(Student::all()->keyBy->id);
// });